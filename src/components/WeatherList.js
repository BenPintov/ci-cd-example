import React, { useState } from 'react'
import axios from 'axios'
import Conditions from './Conditions.js'



const WeatherList = () => {
    
    let [response, setResponse] = useState([{}])
    // const [responseMade, setResponseMade] = useState(false)
    const [city, setCity] = useState('')
    const [unit, setUnit] = useState('imperial')
    console.log(response.data)

    function getForecast() {
        axios.request(options).then(res => {
            console.log("AXIOS RES", res)
            setResponse(res);
        })
    }

    // const changeUnit = (e) => {
    //     e.preventDefault();
    // }

    var options = {
        method: 'GET',
        url: 'https://community-open-weather-map.p.rapidapi.com/weather',
        params: {
          q: city,
          lat: '0',
          lon: '0',
          callback: '',
          id: '2172797',
          lang: 'null',
          units: unit,
          mode: 'json, html'
        },
        headers: {
          'x-rapidapi-key': '66b0fc4e49mshf977a8e1819cdb8p145e72jsne6b7049b96a4',
          'x-rapidapi-host': 'community-open-weather-map.p.rapidapi.com'
        }
      };
        
    return (
        <div className='options'>
            <input type='text' placeholder="Type your city here" onInput={e => {
                setCity(e.target.value)
                options.params.city = e.target.value
                console.log(options.params)
            }}/>
            <div className='radios'>
            <input type='radio' name='unitInput' value='imperial' onInput={e => { 
                setUnit(e.target.value)
                options.params.units = e.target.value
                console.log(options.params)
            }
                } /> Imperial 
            <input type='radio' name='unitInput' value='metric' onInput={e => {
                setUnit(e.target.value)
                options.params.units = e.target.value
                console.log(options.params)
            }
                } /> Metric 
            </div>
            <button onClick={getForecast}>Check the Weather!</button>
            {response.data ?
            <Conditions response={response} />
            : null
            }
            
        </div>
    )
}

export default WeatherList
