// import logo from './logo.svg';
import './App.css';
import Header from './components/Header'
// import WeatherList from './components/WeatherList'
import { useEffect } from 'react';



function App() {
  
  useEffect(() => {
    document.title = 'Pini Weather';
  })


  return (
    <div className="App">
      <div className='logo'>B.P <br /> Solutions</div>
      <Header />
      

    </div>
  );
}

export default App;
