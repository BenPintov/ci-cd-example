import React from 'react'
import WeatherList from './WeatherList'

const Header = () => {
    return (
        <div className='headers'>
            <h1>Ben Weather</h1>
            <WeatherList />
        </div>
    )
}

 export default Header
