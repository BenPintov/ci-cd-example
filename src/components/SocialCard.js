import React from 'react'

const SocialCard = () => {
    return (
        <div className='cardHead'>
            <div className='logo'>B.P <br /> Solutions</div>
            <h1>Ben Pintov</h1>
            <h2>Social Engineer</h2>
        </div>
    )
}

export default SocialCard
